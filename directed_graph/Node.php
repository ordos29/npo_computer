<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.19
 * Time: 17:03
 */

class Node
{
    private $breadcrumbs = [];
    private $edges = [];
    private $isVisited = false;
    private $number;

    public function __construct($number)
    {
        $this->number = $number;
        $this->breadcrumbs[] = $number;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @return array
     */
    public function getBreadcrumbs(): array
    {
        return $this->breadcrumbs;
    }

    public function addBreadcrumbs(array $breadcrumbs)
    {
        $breadcrumbs[] = $this->number;
        $this->breadcrumbs = $breadcrumbs;

        return $this;
    }

    /**
     * @return int[]
     */
    public function getEdges(): array
    {
        return $this->edges;
    }

    /**
     * @return bool
     */
    public function isVisited(): bool
    {
        return $this->isVisited;
    }

    /**
     * @param bool $isVisited
     * @return Node
     */
    public function setIsVisited(bool $isVisited): Node
    {
        $this->isVisited = $isVisited;
        return $this;
    }

    public function addEdge(int $edge): self
    {
        if (!in_array($edge, $this->edges, true)) {
            $this->edges[] = $edge;
        }

        return $this;
    }

}
