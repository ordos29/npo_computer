<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 17.03.19
 * Time: 17:13
 */

class PathFinder
{
    /** @var Node[] */
    private $graph = [];
    /** @var Node[] */
    private $currentNodes = [];
    /** @var int */
    private $finishNodeNumber;

    public function __construct(int $startNode, int $finishNode, array $edges)
    {
        $this->finishNodeNumber = $finishNode;
        foreach ($edges as $edge) {
            $this->getNode($edge[0])->addEdge($edge[1]);
        }
        $this->currentNodes[$startNode] = $this->getNode($startNode);
    }

    public function findPath(): ?array
    {
        do {
            $nextStepNodes = [];
            foreach ($this->currentNodes as $node) {
                /** @var Node $node */
                $nextStepNodes += $this->makeStep($node);
                if ($node->getNumber() === $this->finishNodeNumber) {
                    return $node->getBreadcrumbs();
                }
            }
            $this->currentNodes = $nextStepNodes;
        } while ($this->notAllNodesVisited() && count($nextStepNodes) > 0);

        return ['Path not found'];
    }

    public function makeStep(Node $node): array
    {
        $nextStepNodes = [];
        foreach ($node->getEdges() as $nodeNumber) {
            $successorNode = $this->getNode($nodeNumber);
            if (!$successorNode->isVisited()) {
                $successorNode->setIsVisited(true);
                $successorNode->addBreadcrumbs($node->getBreadcrumbs());
                $nextStepNodes[$successorNode->getNumber()] = $successorNode;
            }
        }

        return $nextStepNodes;
    }

    private function getNode(int $index): Node
    {
        if (isset($this->graph[$index])) {
            $node = $this->graph[$index];
        } else {
            $node = new Node($index);
            $this->graph[$index] = $node;
        }

        return $node;
    }

    private function notAllNodesVisited(): bool
    {
        foreach ($this->graph as $node) {
            if ($node->isVisited()) {
                return true;
            }
        }

        return false;
    }

}
