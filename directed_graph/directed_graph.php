<?php
require 'Node.php';
require 'PathFinder.php';
[$start, $end, $edges] = loadEdges();

$pathFinder = new PathFinder($start, $end, $edges);
$answer = $pathFinder->findPath();
if ($answer !== false) {
    writeAnswer($answer);
}

function loadEdges(): array
{
    $input = file_get_contents(__DIR__ . '/input.txt');
    $inputData = explode(PHP_EOL, $input);
    $header = explode(' ', array_shift($inputData));
    $edges = array_map(function ($row) {
        return explode(' ', $row);
    }, $inputData);

    return [$header[1], $header[2], $edges];
}


function writeAnswer(array $answer): void
{
    file_put_contents(__DIR__ . '/output.txt', implode(' ', $answer));
}
