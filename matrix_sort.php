<?php

$matrix = getMatrix();
$matrix = getSortedMatrix($matrix);
$matrix = getRotatedMatrix($matrix);
$matrix = getSortedMatrix($matrix);
$matrix = getRotatedMatrix($matrix);
writeAnswer($matrix);

function writeAnswer(array $matrix): void
{
    $buffer = [];
    foreach ($matrix as $row) {
        $buffer[] = implode(' ', $row);

    }
    file_put_contents(__DIR__ . '/output.txt', implode(PHP_EOL, $buffer));
}

function getRotatedMatrix(array $matrix): array
{
    $rotatedMatrix = [];
    $matrixRowSize = count($matrix[0]);
    for ($i = 0; $i <= $matrixRowSize; $i++) {
        $rotatedMatrix[] = array_column($matrix, $i);
    }

    return $rotatedMatrix;
}

function getSortedMatrix(array $matrix): array
{
    $lengths = [];
    $sortedMatrix = [];
    foreach ($matrix as $index => $row) {
        $lengths[$index] = findMaxSequencesLength($row);
    }
    arsort($lengths);
    foreach ($lengths as $index => $length) {
        $sortedMatrix[] = $matrix[$index];
    }

    return $sortedMatrix;
}

function getMatrix(): array
{
    $input = file_get_contents(__DIR__ . '/input.txt');
    $matrix = explode(PHP_EOL, $input);
    array_shift($matrix);

    return array_map(function ($row) {
        return explode(' ', $row);
    }, $matrix);
}

function findMaxSequencesLength(array $sequence): int
{
    $generalDirection = null;
    $currentDirection = null;
    $maxLength = 0;
    $currentLength = 0;
    $prevItem = null;
    foreach ($sequence as $item) {
        if ($prevItem === null) {
            $prevItem = $item;
        }
        $currentDirection = $prevItem <=> $item;
        if ($generalDirection === null) {
            $generalDirection = $currentDirection;
        }

        if ($currentDirection === $generalDirection || $currentDirection === 0 || $generalDirection === 0) {
            $currentLength++;
            if ($currentDirection !== 0) {
                $generalDirection = $currentDirection;
            }
        } else {
            $maxLength = $currentLength < $maxLength ? $maxLength : $currentLength;
            $currentLength = 2;
            $generalDirection = $currentDirection;
        }
        $prevItem = $item;
    }


    return $maxLength < $currentLength ? $currentLength : $maxLength;
}
